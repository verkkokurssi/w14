import React , { Component}  from 'react';
import './App.css';

function App() {
  //here is an example how to use localStorage, see https://www.w3schools.com/jsref/prop_win_localstorage.asp

  let chats = [];

  //function reads data from localStorage, before the read you need to write something
  function read(){
    const myValueInLocalStorage = localStorage.getItem("myValue");

    chats = JSON.parse(myValueInLocalStorage);
    document.getElementById("example").value = chats[chats.length - 1];  

    chats.map((chat) => {
      console.log(chat);
    });
  }

  //function writes data to localStorage
  function write(){
    chats.push(document.getElementById("example").value);
    let string = JSON.stringify(chats);

    localStorage.setItem("myValue", string);

    // Clear message
    document.getElementById("example").value = "";
  }

  return (
    <div className="App">
      <h1>Chat assignment</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Urna condimentum mattis pellentesque id nibh tortor id aliquet. Posuere urna nec tincidunt praesent semper feugiat nibh. Id aliquet risus feugiat in ante metus. Sed turpis tincidunt id aliquet risus feugiat in ante. Vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt. Et odio pellentesque diam volutpat commodo sed egestas egestas. Tortor at auctor urna nunc id cursus metus aliquam eleifend. Lobortis mattis aliquam faucibus purus. Mauris pharetra et ultrices neque. Elementum integer enim neque volutpat ac tincidunt vitae semper quis. In pellentesque massa placerat duis ultricies lacus sed turpis. Tellus cras adipiscing enim eu turpis egestas pretium aenean pharetra. Suspendisse ultrices gravida dictum fusce ut placerat orci nulla.</p>
      <p>Eleifend donec pretium vulputate sapien nec sagittis aliquam malesuada bibendum. Enim ut sem viverra aliquet eget sit amet tellus cras. Tortor aliquam nulla facilisi cras fermentum odio eu feugiat pretium. Sed blandit libero volutpat sed cras ornare arcu. Lacus vel facilisis volutpat est velit. Vel risus commodo viverra maecenas. Sagittis eu volutpat odio facilisis mauris. Rutrum tellus pellentesque eu tincidunt tortor aliquam nulla. Malesuada bibendum arcu vitae elementum. Velit aliquet sagittis id consectetur.</p>
      <p>Et magnis dis parturient montes nascetur ridiculus. Diam sit amet nisl suscipit adipiscing bibendum est ultricies. Porttitor lacus luctus accumsan tortor. Ultrices tincidunt arcu non sodales neque sodales. Egestas sed sed risus pretium quam. Risus sed vulputate odio ut enim blandit volutpat. Fermentum dui faucibus in ornare quam viverra orci sagittis. Tempor id eu nisl nunc mi ipsum faucibus vitae. Ipsum a arcu cursus vitae. Faucibus et molestie ac feugiat sed. Nisl suscipit adipiscing bibendum est. Egestas diam in arcu cursus euismod quis viverra nibh cras. Duis at tellus at urna condimentum mattis. Arcu risus quis varius quam quisque. Nunc sed id semper risus in.</p>
      <p>Commodo odio aenean sed adipiscing diam donec adipiscing tristique. Felis bibendum ut tristique et egestas quis ipsum. Laoreet id donec ultrices tincidunt arcu non sodales. Nisl nunc mi ipsum faucibus vitae aliquet. Euismod elementum nisi quis eleifend quam adipiscing vitae. Quis hendrerit dolor magna eget est lorem. Suscipit tellus mauris a diam maecenas sed enim. Sit amet consectetur adipiscing elit duis tristique sollicitudin. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus aenean. Sit amet justo donec enim diam. Diam quis enim lobortis scelerisque fermentum dui faucibus. Aliquam purus sit amet luctus venenatis lectus. Amet cursus sit amet dictum. Et netus et malesuada fames. Turpis egestas integer eget aliquet. In iaculis nunc sed augue lacus.</p>
      <p>Porttitor eget dolor morbi non. Curabitur vitae nunc sed velit dignissim sodales ut eu sem. Arcu odio ut sem nulla pharetra. Etiam erat velit scelerisque in dictum non. In arcu cursus euismod quis viverra nibh cras. Mollis nunc sed id semper risus. Vitae nunc sed velit dignissim sodales ut. Blandit volutpat maecenas volutpat blandit aliquam. Lobortis feugiat vivamus at augue eget arcu dictum varius. Iaculis nunc sed augue lacus viverra vitae. Integer malesuada nunc vel risus commodo viverra maecenas. Non enim praesent elementum facilisis leo. Tellus id interdum velit laoreet id. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Tincidunt dui ut ornare lectus. Blandit turpis cursus in hac habitasse platea dictumst quisque. Feugiat pretium nibh ipsum consequat nisl. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Ut consequat semper viverra nam libero. Odio tempor orci dapibus ultrices in iaculis.</p>
      <br></br>
      <b>Chat</b><br></br>

      <input type="text" id="example"></input>
      <button onClick={write}>Send</button><br></br>
      <button onClick={read}>Show messages</button>
    </div>
  );
}

export default App;
